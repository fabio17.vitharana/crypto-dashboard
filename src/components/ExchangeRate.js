function ExchangeRate({ exchangeData}) {
    //exchangeRate = parseFloat(exchangeRate,10).toFixed(2); if you want to have the rate to 2 decimal places

    return (
        <>
            <div className="exchange-rate">
                <div>Exchange rate</div>
                <div>{exchangeData.exchangeRate}</div>
                <div className="exchange-currency">
                    {exchangeData.primaryCurrency} to {exchangeData.secondaryCurrency}
                </div>
            </div>
        </>
    )
}

export default ExchangeRate;