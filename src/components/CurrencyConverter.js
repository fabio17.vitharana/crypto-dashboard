import { useState } from 'react';
import ExchangeRate from './ExchangeRate';
import axios from 'axios';

function CurrencyConverter() {

    const currencies = ['BTC', 'ETH', 'XRP', 'LTC', 'ADA', 'EUR'];
    const [chosenPrimaryCurrency, setChosenPrimaryCurrency] = useState('BTC');
    const [chosenSecondaryCurrency, setChosenSecondaryCurrency] = useState('USD');
    const [amount, setAmount] = useState(1);
    //const [exchangeRate, setExchangeRate] = useState(0); not needed anymore since we have exchangeDate

    const [exchangeData, setExchangeData] = useState({
        primaryCurrency: 'BTC',
        secondaryCurrency: 'USD',
        exchangeRate: 0,
    });

    const [result, setResult] = useState("");

    function convert() {


        const options = {
            method: 'GET',
            url: 'https://alpha-vantage.p.rapidapi.com/query',
            params: { to_currency: chosenSecondaryCurrency, function: 'CURRENCY_EXCHANGE_RATE', from_currency: chosenPrimaryCurrency },
            headers: {
                'X-RapidAPI-Key': process.env.REACT_APP_RAPID_API_KEY,
                'X-RapidAPI-Host': 'alpha-vantage.p.rapidapi.com'
            }
        };

        axios.request(options).then((response) => {
            const rate = response.data['Realtime Currency Exchange Rate']['5. Exchange Rate']
            //setExchangeRate(rate)
            setResult(amount * rate)
            setExchangeData({
                primaryCurrency: chosenPrimaryCurrency,
                secondaryCurrency: chosenSecondaryCurrency,
                exchangeRate: rate,
            })
        }).catch((error) => {
            console.error(error);
        });


    }



    return (
        <>
            <div className="currency-converter">
                <h2 className='heading'>Crypto Currency Converter</h2>

                <div className="input-table">

                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    Primary Currency:
                                </td>
                                <td>
                                    <input
                                        type="number"
                                        name="currency-amount-1"
                                        value={amount}
                                        className='currency-input'
                                        onChange={(e) => setAmount(e.target.value)}
                                    />
                                </td>
                                <td>
                                    <select
                                        name="currency-option-1"
                                        defaultValue={chosenPrimaryCurrency}
                                        className="currency-options"
                                        onChange={(e) => setChosenPrimaryCurrency(e.target.value)}
                                    >
                                        <option value="#">Choose a currency</option>

                                        {currencies.map((currency, _index) => (
                                            <option key={_index}>{currency}</option>
                                        ))}

                                    </select>
                                </td>
                            </tr>

                            <tr className='currency-row'>
                                <td>
                                    Secondary Currency:
                                </td>
                                <td>
                                    <input
                                        type="number"
                                        name="currency-amount-2"
                                        className='currency-input'
                                        value={result}
                                        disabled={true}
                                    />
                                </td>
                                <td>
                                    <select
                                        name="currency-option-2"
                                        defaultValue={chosenSecondaryCurrency}
                                        className="currency-options"
                                        onChange={(e) => setChosenSecondaryCurrency(e.target.value)}
                                    >
                                        <option value="#">Choose a currency</option>
                                        <option key="USD">USD</option>
                                        {currencies.map((currency, _index) => (
                                            <option key={_index} >{currency}</option>
                                        ))}
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <button id="convert-btn" onClick={convert} className='currency-btn'>Convert</button>



                </div>


                <ExchangeRate
                    exchangeData={exchangeData}
                />

            </div>
        </>
    )
}

export default CurrencyConverter;