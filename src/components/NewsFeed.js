import { useEffect, useState } from "react";
import axios from 'axios';

function NewsFeed() {

    const [articles, setArticles] = useState(null);

    useEffect(() => {

        const options = {
            method: 'GET',
            url: 'https://crypto-news-today.p.rapidapi.com/news',
            params: { start_date: '2020-01-01', end_date: '2020-01-08' },
            headers: {
                'X-RapidAPI-Key': process.env.REACT_APP_RAPID_API_KEY,
                'X-RapidAPI-Host': 'crypto-news-today.p.rapidapi.com'
            }
        };

        axios.request(options).then(function (response) {
            console.log(response.data);
            setArticles(response.data.news);
        }).catch(function (error) {
            console.error(error);
        });
    }, []);

   // console.log(articles);

    const first7Articles = articles?.slice(0, 7); // question mark is used to check whether the array exists, slice() cuts a number of elements from the array

    return (
        <>
            <div className="news-feed">
                <h2 className='heading'>News Feed</h2>

                {first7Articles?.map((article, _index) => {
                    return (
                        <div key={_index} className="news-item">
                            <a href={article.url}>
                                <p>{article.title}</p>
                            </a>
                        </div>
                    )
                })}
            </div>

        </>
    )

}

export default NewsFeed;